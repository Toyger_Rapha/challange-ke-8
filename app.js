const express = require('express')
const app = express()
const cors = require('cors')
const apiRouter = require('./server/routes')
const errorHandler = require('./server/middlewares/errorHandler')
const swaggerUi = require("swagger-ui-express");
const swaggerJSON = require('./Swagger_Open_API_CH8.json')
const PORT = process.env.PORT || 4000

// middlewares
app.use(cors())
app.use(express.urlencoded({extended: true}))
app.use(express.json())
app.use(errorHandler)

/**
 * @Routes /api
 * entrypoint for all API routes
 */
app.use("/api", apiRouter)
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerJSON));

app.listen(PORT, () => {
  console.log(`Listening on http://localhost:${PORT}`)
})